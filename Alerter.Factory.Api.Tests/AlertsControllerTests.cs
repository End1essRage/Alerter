using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Alerter.Factory.Api.Data;
using Alerter.Factory.Api.HttpClients;
using AutoMapper;
using Alerter.Factory.Api.Controllers;
using Microsoft.AspNetCore.Mvc;
using Alerter.Factory.Api.Models;
using Alerter.Factory.Api.Dtos;
using AutoFixture;
using Microsoft.AspNetCore.Http;

namespace Alerter.Factory.Api.Tests
{
    public class AlertsControllerTests
    {
        private Mock<IMapper> mapperMock;
        private Mock<IAlertRepo> repositoryMock;
        private Mock<IHashServiceClient> hashServiceMock;
        private AlertsController controller;

        public AlertsControllerTests()
        {
            mapperMock = new Mock<IMapper>();
            repositoryMock = new Mock<IAlertRepo>();
            hashServiceMock = new Mock<IHashServiceClient>();
            controller = new AlertsController(mapperMock.Object,
                repositoryMock.Object,
                hashServiceMock.Object);
        }

        [Fact]
        public void IsAlive_ShouldPass()
        {
            Assert.True(true);
        }

        [Fact]
        public async Task GetAlertById_ReturnsNotFound_WhenItemNotExists()
        {
            //arrange
            int id = 1;
            repositoryMock.Setup(r => r.GetByIdAsync(id)).Returns(Task.FromResult<Alert>(null));
            //act
            var response = await controller.GetAlertById(id);
            //assert
            Assert.IsType<NotFoundResult>(response.Result);
        }

        [Fact]
        public async Task GetAlertById_ReturnsItem_WhenItemExists()
        {
            //arrange
            var alert = new Fixture().Create<Alert>();
            var alertReadDto = new Fixture().Build<AlertReadDto>().With(a => a.Id, alert.Id).Create();
            
            repositoryMock.Setup(r => r.GetByIdAsync(alert.Id)).Returns(Task.FromResult(alert));
            mapperMock.Setup(m => m.Map<AlertReadDto>(alert)).Returns(alertReadDto);
            //act
            var response = await controller.GetAlertById(alert.Id);
            //assert
            var actionResult = Assert.IsType<OkObjectResult>(response.Result);
            Assert.Equal(alertReadDto, actionResult.Value);
        }
        [Fact]
        public async Task GetAlertByUid_ReturnsBadRequest_WhenUidIsEmptyString()
        {
            //arrange
            string uid = "";
            //act
            var response = await controller.GetAlertByUid(uid);
            //assert
            Assert.IsType<BadRequestResult>(response.Result);
        }
        [Fact]
        public async Task GetAlertByUid_ReturnsItem_WhenItemExists()
        {
            //arrange
            var alert = new Fixture().Create<Alert>();
            var alertReadDto = new Fixture().Build<AlertReadDto>().With(a => a.Uid, alert.Uid).Create();

            repositoryMock.Setup(r => r.GetByUidAsync(alert.Uid)).Returns(Task.FromResult(alert));
            mapperMock.Setup(m => m.Map<AlertReadDto>(alert)).Returns(alertReadDto);
            //act
            var response = await controller.GetAlertByUid(alert.Uid);
            //assert
            var actionResult = Assert.IsType<OkObjectResult>(response.Result);
            Assert.Equal(alertReadDto, actionResult.Value);
        }
        [Fact]
        public async Task GetAlertByUid_ReturnsNotFound_WhenItemNotExists()
        {
            //arrange
            string uid = "";
            repositoryMock.Setup(r => r.GetByUidAsync(uid)).Returns(Task.FromResult<Alert>(null));
            //act
            var response = await controller.GetAlertByUid(uid);
            //assert
            Assert.IsType<NotFoundResult>(response.Result);
        }
        [Fact]
        public async Task CreateAlert_ReturnsServiceUnavailable_WhenServiceThrowsException()
        {
            //arrange
            var createDto = new Fixture().Create<AlertCreateDto>();
            hashServiceMock.Setup(c => c.RequestNewHash()).Throws<Exception>();
            //act
            var response = await controller.CreateAlert(createDto) as ObjectResult;
            //assert
            Assert.Equal(StatusCodes.Status503ServiceUnavailable, response.StatusCode);
        }
        [Fact]
        public async Task CreateAlert_ReturnsBadRequest_WhenDtoIsInvalid()
        {
            //arrange
            var createDto = new Fixture().Build<AlertCreateDto>()
                .With(c => c.Name,"")
                .With(c => c.Message, "")
                .Create();
            //act
            var response = await controller.CreateAlert(createDto);
            //assert
            Assert.IsType<BadRequestResult>(response);
        }
        [Fact]
        public async Task CreateAlert_ReturnsInternalServerError_WhenDbNotSave()
        {
            //arrange
            var alert = new Fixture().Create<Alert>();
            var createDto = new Fixture().Create<AlertCreateDto>();
            var readDto = new Fixture().Create<AlertReadDto>();

            mapperMock.Setup(m => m.Map<Alert>(createDto)).Returns(alert);
            mapperMock.Setup(m => m.Map<AlertReadDto>(alert)).Returns(readDto);

            repositoryMock.Setup(r => r.CreateNew(alert));
            repositoryMock.Setup(r => r.SaveChangesAsync()).Returns(Task.FromResult(false));
            //act
            //bagged? Throws null reference
            var response = await controller.CreateAlert(createDto) as ObjectResult;
            //assert
            Assert.Equal(StatusCodes.Status500InternalServerError, response.StatusCode);
        }
        [Fact]
        public async Task CreateAlert_ReturnsCreated_WhenDbSave()
        {
            //arrange
            var alert = new Fixture().Create<Alert>();
            var createDto = new Fixture().Create<AlertCreateDto>();
            var readDto = new Fixture().Create<AlertReadDto>();

            mapperMock.Setup(m => m.Map<Alert>(createDto)).Returns(alert);
            mapperMock.Setup(m => m.Map<AlertReadDto>(alert)).Returns(readDto);

            repositoryMock.Setup(r => r.CreateNew(alert));
            repositoryMock.Setup(r => r.SaveChangesAsync()).Returns(Task.FromResult(true));
            //act
            var response = await controller.CreateAlert(createDto);
            //assert
            Assert.IsType<CreatedAtRouteResult>(response);
        }
    }
}