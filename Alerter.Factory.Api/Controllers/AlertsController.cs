using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alerter.Factory.Api.Data;
using Alerter.Factory.Api.HttpClients;
using Alerter.Factory.Api.Dtos;
using Alerter.Factory.Api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Alerter.Factory.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AlertsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IAlertRepo _repository;
        private readonly IHashServiceClient _hashService;

        public AlertsController(
            IMapper mapper,
            IAlertRepo repository,
            IHashServiceClient hashService)
        {
            _mapper = mapper;
            _repository = repository;
            _hashService = hashService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<AlertReadDto>>> GetAllAlerts()
        {
            Console.WriteLine("--> Hit GetAllAlerts");
            //dont need tests ef core functionallity
            var alerts = await _repository.GetAllAsync();
            //its ok returning empty collection?
            return Ok(_mapper.Map<IEnumerable<AlertReadDto>>(alerts));
        }

        [HttpGet("{id}", Name = "GetAlertById")]
        public async Task<ActionResult<AlertReadDto>> GetAlertById(int id)
        {
            Console.WriteLine($"--> Hit GetAlertById with id {id}");

            var alert = await _repository.GetByIdAsync(id);
            //Can test response code
            if (alert == null)
                return NotFound();

            return Ok(_mapper.Map<AlertReadDto>(alert));
        }

        [HttpGet("uid", Name = "GetAlertByUid")]
        public async Task<ActionResult<AlertReadDto>> GetAlertByUid([FromQuery] string uid)
        {
            Console.WriteLine($"--> Hit GetAlertByUid with uid {uid}");

            var alert = await _repository.GetByUidAsync(uid);
            //Can test response code
            if (alert == null)
                return NotFound();

            return Ok(_mapper.Map<AlertReadDto>(alert));
        }

        [HttpPost] //������� ������ ���� ������ � �� ���������
        public async Task<ActionResult> CreateAlert(AlertCreateDto alertCreateDto)
        {
            Console.WriteLine("--> Hit CreateAlert");

            var alertModel = _mapper.Map<Alert>(alertCreateDto);
            string uid = string.Empty;

            try
            {
                uid = await _hashService.RequestNewHash();
            }

            catch (Exception e)
            {
                Console.WriteLine($"--> Error with requesting hash {e.Message}");

                return BadRequest(e.Message);
            }

            if (uid == string.Empty)
            {
                //Change and Test responses
                //Cant Request Hash HashClient bugged
                return BadRequest();
            }

            Console.WriteLine($"--> uid is {uid}");

            alertModel.Uid = uid;
            _repository.CreateNew(alertModel);

            var savingResult = await _repository.SaveChangesAsync();

            if (!savingResult)
            {
                //Change and Test responses
                //Cant Save Changes to db
                Console.WriteLine("--> Cant save entities to db");
                return BadRequest();
            }

            var alertReadDto = _mapper.Map<AlertReadDto>(alertModel);

            return CreatedAtRoute(nameof(GetAlertById), new { Id = alertReadDto.Id }, alertReadDto);
        }
    }
}