using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alerter.Factory.Api.Dtos;
using Alerter.Factory.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Alerter.Factory.Api.Data
{
    public class AlertRepo : IAlertRepo
    {
        private readonly AppDbContext _context;
        public AlertRepo(AppDbContext context)
        {
            _context = context;
        }

        public void CreateNew(Alert alert)
        {
            if (alert == null)
                throw new ArgumentNullException(nameof(alert));

            _context.Alerts.Add(alert);
        }

        public async Task<IEnumerable<Alert>> GetAllAsync()
        {
            return await _context.Alerts.ToListAsync();
        }

        public async Task<Alert> GetByUidAsync(string uid)
        {
            return await _context.Alerts.SingleOrDefaultAsync(a => a.Uid == uid);
        }

        public async Task<Alert> GetByIdAsync(int id)
        {
            return await _context.Alerts.SingleOrDefaultAsync(a => a.Id == id);
        }

        public async Task<bool> SaveChangesAsync()
        {
            var rows = await _context.SaveChangesAsync();

            Console.WriteLine($"Updated {rows} rows");

            return rows > 0;
        }
    }
}