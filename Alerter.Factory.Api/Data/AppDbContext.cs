using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alerter.Factory.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Alerter.Factory.Api.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alert>()
                .HasIndex(a => a.Uid);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Alert> Alerts { get; set; }
    }
}