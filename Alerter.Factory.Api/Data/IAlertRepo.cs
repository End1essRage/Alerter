using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alerter.Factory.Api.Dtos;
using Alerter.Factory.Api.Models;

namespace Alerter.Factory.Api.Data
{
    public interface IAlertRepo
    {
        public Task<IEnumerable<Alert>> GetAllAsync();
        public Task<Alert> GetByIdAsync(int id);
        public Task<Alert> GetByUidAsync(string uid);
        public void CreateNew(Alert alert);
        public Task<bool> SaveChangesAsync();
    }
}