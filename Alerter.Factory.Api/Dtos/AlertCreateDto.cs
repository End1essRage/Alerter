using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alerter.Factory.Api.Dtos
{
    public class AlertCreateDto
    {
        public string Name { get; set; }
        public string Message { get; set; }
    }
}