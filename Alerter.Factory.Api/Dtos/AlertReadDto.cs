using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alerter.Factory.Api.Dtos
{
    public class AlertReadDto
    {
        public int Id { get; set; }
        public string Uid { get; set; } //for debug
        public string Name { get; set; }
        public string Message { get; set; }
    }
}