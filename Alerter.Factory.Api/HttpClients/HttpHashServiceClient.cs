using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alerter.Factory.Api.HttpClients
{
    public class HttpHashServiceClient : IHashServiceClient
    {
        private readonly HttpClient _client;
        private readonly IConfiguration _configuration;

        public HttpHashServiceClient(HttpClient client, IConfiguration configuration)
        {
            _client = client;
            _configuration = configuration;
        }

        //How to test it??
        //throw error if request != 2xx
        //return string if its ok
        public async Task<string> RequestNewHash()
        {
            Console.WriteLine("--> Trying to send request to hashservice");
            var responsePost = await _client.PostAsync($"{_configuration.GetSection("HttpUrls:HashService").Value}/api/hash", null);
            if (!responsePost.IsSuccessStatusCode)
            {
                //Throw error unavailable
                throw new Exception("POST request to HashService UnSuccessfull");
            }

            Console.WriteLine("--> Sync POST to HashService was OK");
            var data = await responsePost.Content.ReadAsStringAsync();
            Console.WriteLine($"--> Sync POST response is {data}");
            return data;
        }
    }
}