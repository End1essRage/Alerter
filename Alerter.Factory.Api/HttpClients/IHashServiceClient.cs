using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alerter.Factory.Api.HttpClients
{
    public interface IHashServiceClient
    {
        public Task<string> RequestNewHash();
    }
}