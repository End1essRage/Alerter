using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Alerter.Factory.Api.Models
{
    [Index(nameof(Uid))]
    public class Alert
    {
        public int Id { get; set; }
        public string Uid { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
    }
}