using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alerter.Factory.Api.Dtos;
using Alerter.Factory.Api.Models;
using AutoMapper;

namespace Alerter.Factory.Api.Profiles
{
    public class AlertsProfile : Profile
    {
        public AlertsProfile()
        {
            CreateMap<Alert, AlertReadDto>();
            CreateMap<AlertCreateDto, Alert>();
        }
    }
}