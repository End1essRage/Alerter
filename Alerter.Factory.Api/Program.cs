using Alerter.Factory.Api.Data;
using Alerter.Factory.Api.HttpClients;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<AppDbContext>(opt =>
{
    opt.UseInMemoryDatabase("Alerts");
});

builder.Services.AddScoped<IAlertRepo, AlertRepo>();
builder.Services.AddHttpClient<IHashServiceClient, HttpHashServiceClient>();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

Console.WriteLine($"--> HashService ip is {builder.Configuration.GetSection("HttpUrls:HashService").Value}");

app.UseAuthorization();

app.MapControllers();

app.Run();
