using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using alerter.hash.service.Data;
using Alerter.Hash.Service.Data;
using Microsoft.AspNetCore.Mvc;

namespace Alerter.Hash.Service.Controllers
{
    [ApiController]
    [Route("api/hash")]
    public class HashContoller : ControllerBase
    {
        private readonly IHashRepo _repository;
        public HashContoller(IHashRepo repository)
        {
            _repository = repository;
        }

        [HttpPost]
        public ActionResult<string> PostNewHash()
        {
            Console.WriteLine("--> Incoming POST Request GetNewHash");

            var key = _repository.GetNextKey();

            Console.WriteLine($"--> New key is {key}");

            return Ok(EncryptKey(key));
        }

        private string EncryptKey(long key)
        {
            var bytes = Encoding.UTF8.GetBytes(key.ToString());
            return Convert.ToBase64String(bytes);
        }
    }
}