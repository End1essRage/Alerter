using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using alerter.hash.service.Data;

namespace Alerter.Hash.Service.Data
{
    public class FakeHashRepo : IHashRepo
    {
        public long GetNextKey()
        {
            var random = new Random();

            return random.NextInt64();
        }
    }
}