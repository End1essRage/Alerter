using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alerter.Hash.Service.Data;
using Microsoft.EntityFrameworkCore;

namespace alerter.hash.service.Data
{
    public class HashRepo : IHashRepo
    {
        private readonly AppDbContext _context;
        public HashRepo(AppDbContext context)
        {
            _context = context;
        }
        public long GetNextKey()
        {
            Console.WriteLine("--> Getting new Number");
            char quot = '"';
            long value = -1;

            /*
            SELECT t."Value"
            FROM (
                select nextval('keys_sequence') as "Value" //doesnt works without "as "Value"" ))00)))0
            ) AS t
            LIMIT 1
            */

            //нужно ли и каким образом тестировать запрос или нужен интеграционный тест с таблицей?
            try
            {
                value = _context.Database.SqlQueryRaw<long>($"SELECT nextval('keys_sequence') as {quot}Value{quot}").FirstOrDefault();
            }
            catch (Exception e)
            {
                Console.WriteLine($"--> Cant Execute Sql query {e.Message}");
            }
            //Нужно ли проверять выброс Exception
            if (value == -1)
                throw new Exception("Cant get sequence value from DataBase");

            return value;
        }
    }
}