using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace alerter.hash.service.Data
{
    public interface IHashRepo
    {
        public long GetNextKey();
    }
}