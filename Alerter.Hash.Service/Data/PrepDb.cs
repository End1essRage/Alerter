using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Alerter.Hash.Service.Data
{
    public static class PrepDb
    {
        public static void CreateSequence(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                Console.WriteLine("--> Creating sequence");

                var context = serviceScope.ServiceProvider.GetService<AppDbContext>();
                if (context != null)
                {
                    try
                    {
                        var result = context.Database.ExecuteSql($"CREATE SEQUENCE IF NOT EXISTS keys_sequence");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"--> Creating sequence ERROR {e.Message}");
                    }
                }
            }
        }
    }
}