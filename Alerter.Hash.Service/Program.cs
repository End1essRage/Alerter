using alerter.hash.service.Data;
using Alerter.Hash.Service.Data;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

Console.WriteLine($"--> Builder Environment is Production {builder.Environment.IsProduction()}");

// Add services to the container.
if (builder.Environment.IsProduction())
{
    var conString = Environment.GetEnvironmentVariable("postgressConnection");

    Console.WriteLine($"--> Connection string is {conString}");

    builder.Services.AddDbContext<AppDbContext>(opt =>
    {
        opt.UseNpgsql(conString);
    });

    builder.Services.AddScoped<IHashRepo, HashRepo>();
}
else
{
    builder.Services.AddDbContext<AppDbContext>(opt =>
    {
        opt.UseInMemoryDatabase("InMemory");
    });
    builder.Services.AddScoped<IHashRepo, FakeHashRepo>();
}

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

Console.WriteLine($"--> App Environment is Production {app.Environment.IsProduction()}");

if (app.Environment.IsProduction())
{
    PrepDb.CreateSequence(app);
    Console.WriteLine("--> Sequence CREATED");
}

app.Run();
