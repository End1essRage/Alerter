using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alerter.Subs.Api.Data;
using Alerter.Subs.Api.Dtos;
using Alerter.Subs.Api.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Alerter.Subs.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SubsController : ControllerBase
    {
        private readonly ISubRepo _repository;
        private readonly IMapper _mapper;
        public SubsController(ISubRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<SubReadDto>>> GetAllSubs()
        {
            Console.WriteLine("--> Hitted GetAllSubs");

            var items = await _repository.GetAllAsync();

            return Ok(_mapper.Map<IEnumerable<SubReadDto>>(items));
        }

        [HttpGet("{id}", Name = "GetSubById")]
        public async Task<ActionResult<SubReadDto>> GetSubById(int id)
        {
            Console.WriteLine("--> Hitted GetSubById");

            var item = await _repository.GetByIdAsync(id);

            if (item == null)
                return NotFound();

            return Ok(_mapper.Map<SubReadDto>(item));
        }

        [HttpGet("my")]
        public async Task<ActionResult<IEnumerable<SubReadDto>>> GetMySubscriptions([FromQuery(Name = "email")] string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return BadRequest("email is null");

            var items = await _repository.GetAllByEmailAsync(email);

            return Ok(_mapper.Map<IEnumerable<SubReadDto>>(items));
        }

        [HttpGet("subscribers")]
        public async Task<ActionResult<IEnumerable<SubReadDto>>> GetAlertSubscribers([FromQuery(Name = "alertUid")] string alertUid)
        {
            if (string.IsNullOrWhiteSpace(alertUid))
                return BadRequest("alertUid is null");

            var items = await _repository.GetActiveByAlertUidAsync(alertUid);

            return Ok(_mapper.Map<IEnumerable<SubReadDto>>(items));
        }

        [HttpPost]
        public async Task<ActionResult> CreateNewSub(SubCreateDto subCreateDto)
        {
            Console.WriteLine("--> Hitted CreateNewSub");

            var isExists = await _repository.CheckSubExistsAsync(subCreateDto.AlertUid, subCreateDto.Email);
            if (isExists)
                return BadRequest("Sub already exists");

            var model = _mapper.Map<Subscription>(subCreateDto);
            model.Active = true;

            _repository.CreateSub(model);

            var result = await _repository.SaveChangesAsync();
            if (!result)
                return BadRequest();

            var subReadDto = _mapper.Map<SubReadDto>(model);

            return CreatedAtRoute(nameof(GetSubById), new { Id = subReadDto.Id }, subReadDto);
        }

        [HttpPatch]
        public async Task<ActionResult> SwicthSubActivity(int id)
        {
            Console.WriteLine("--> Hitted SwicthSubActivity");

            var result = await _repository.SwitchSub(id);

            if (!result)
                return BadRequest();

            return Ok();
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteSub(int id)
        {
            Console.WriteLine("--> Hitted DeleteSub");

            _repository.DeleteSub(id);
            var result = await _repository.SaveChangesAsync();

            if (!result)
                return BadRequest();

            return Ok();
        }
    }
}