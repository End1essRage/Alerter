using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alerter.Subs.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Alerter.Subs.Api.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Subscription>()
                .HasIndex(s => s.AlertUid);

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Subscription> Subs { get; set; }
    }
}