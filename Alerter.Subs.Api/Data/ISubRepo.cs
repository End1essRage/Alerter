using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alerter.Subs.Api.Models;

namespace Alerter.Subs.Api.Data
{
    public interface ISubRepo
    {
        public Task<IEnumerable<Subscription>> GetAllAsync();
        public Task<IEnumerable<Subscription>> GetActiveByAlertUidAsync(string alertUid);
        public Task<IEnumerable<Subscription>> GetAllByEmailAsync(string email);
        public Task<Subscription> GetByIdAsync(int id);
        public Task<bool> CheckSubExistsAsync(string alertUid, string email);
        public void CreateSub(Subscription sub);
        public Task<bool> SwitchSub(int id);
        public void DeleteSub(int id);
        public Task<bool> SaveChangesAsync();

    }
}