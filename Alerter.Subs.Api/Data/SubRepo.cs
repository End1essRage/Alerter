using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alerter.Subs.Api.Models;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;

namespace Alerter.Subs.Api.Data
{
    public class SubRepo : ISubRepo
    {
        private readonly AppDbContext _context;
        public SubRepo(AppDbContext context)
        {
            _context = context;
        }

        public async Task<bool> CheckSubExistsAsync(string alertUid, string email)
        {
            return await _context.Subs.AnyAsync(s => s.AlertUid == alertUid && s.Email == email);
        }

        public void CreateSub(Subscription sub)
        {
            if (sub == null)
                throw new ArgumentNullException(nameof(sub));
            _context.Subs.Add(sub);
        }

        public void DeleteSub(int id)
        {
            Subscription sub = new Subscription() { Id = id };
            _context.Subs.Attach(sub);
            _context.Subs.Remove(sub);
        }

        public async Task<IEnumerable<Subscription>> GetAllAsync()
        {
            return await _context.Subs.ToListAsync();
        }

        public async Task<IEnumerable<Subscription>> GetActiveByAlertUidAsync(string alertUid)
        {
            if (string.IsNullOrWhiteSpace(alertUid))
                throw new ArgumentNullException(nameof(alertUid));

            return await _context.Subs.Where(s => s.AlertUid == alertUid && s.Active).ToListAsync();
        }

        public async Task<IEnumerable<Subscription>> GetAllByEmailAsync(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(nameof(email));

            return await _context.Subs.Where(s => s.Email == email).ToListAsync();
        }

        public async Task<Subscription> GetByIdAsync(int id)
        {
            return await _context.Subs.SingleOrDefaultAsync(s => s.Id == id);
        }

        public async Task<bool> SaveChangesAsync()
        {
            var result = await _context.SaveChangesAsync();
            return result >= 0;
        }

        public async Task<bool> SwitchSub(int id)
        {
            var sub = await GetByIdAsync(id);
            if (sub == null)
                throw new ArgumentNullException(nameof(sub));

            sub.Active = !sub.Active;
            _context.Entry(sub).Property(s => s.Active).IsModified = true;

            return await SaveChangesAsync();
        }
    }
}