using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Alerter.Subs.Api.Dtos
{
    public class SubCreateDto
    {
        [Required]
        public string AlertUid { get; set; }
        [Required]
        public string Email { get; set; }
    }
}