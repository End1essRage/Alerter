using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alerter.Subs.Api.Dtos
{
    public class SubReadDto
    {
        public int Id { get; set; }
        public string AlertUid { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
    }
}