using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Alerter.Subs.Api.Models
{
    public class Subscription
    {
        [Key]
        [Column("_id")]
        public int Id { get; set; }
        [Column("alert_id")]
        public string AlertUid { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("active")]
        public bool Active { get; set; }
    }
}