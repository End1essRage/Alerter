using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alerter.Subs.Api.Dtos;
using Alerter.Subs.Api.Models;
using AutoMapper;

namespace Alerter.Subs.Api.Profiles
{
    public class SubsProfile : Profile
    {
        public SubsProfile()
        {
            CreateMap<SubCreateDto, Subscription>();
            CreateMap<Subscription, SubReadDto>();
        }
    }
}