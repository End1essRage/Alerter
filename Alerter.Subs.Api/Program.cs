using Alerter.Subs.Api.Data;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
Console.WriteLine($"--> Builder Environment is Production {builder.Environment.IsProduction()}");

/* FOR MIGRATIONS

builder.Services.AddDbContext<AppDbContext>(opt =>
{
    opt.UseNpgsql("");
});
*/

if (builder.Environment.IsProduction())
{
    Console.WriteLine("--> Connecting to postgres");

    var conString = Environment.GetEnvironmentVariable("postgressConnection");

    builder.Services.AddDbContext<AppDbContext>(opt =>
    {
        opt.UseNpgsql(conString);
    });
}
else
{
    Console.WriteLine("--> Connecting to InMemory");

    builder.Services.AddDbContext<AppDbContext>(opt =>
    {
        opt.UseInMemoryDatabase("InMemory");
    });
}

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddScoped<ISubRepo, SubRepo>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

if (app.Environment.IsProduction())
    PrepDb.Migrate(app);

app.Run();
